from Fuzzy import *

#inputs
input1 = [(0,11,1),'quality']
input2 = [(0,11,1),'service']
#functions of inputs
# function, name, rangue[]
functionsInput1 = [['trimp','low', [0,0,5]],['tramp','medium', [1,3,7,9]],['trimp','high', [5,10,10]]]
functionsInput2 = [['trimp','low',[0,0,4]],['trimp','medium',[2,4,6]],['trimp','high',[4,6,8]],['trimp','veryhigh',[6,10,10]]]
inputs = [input1,input2]
functionsInputs = [functionsInput1,functionsInput2]
#output
output = [(0,13,1),'tip']
#function of output
functionOutput = [['trimp','low',[0,0,12]],['trimp','medium',[0,6,12]],['trimp','high',[0,12,12]]]
#Rules conditional , values[logicalCombination,[Antecedent1,...,AntecedentN,Consequent]]
rules = [['&',['low','low','low']],
         ['&',['low','medium','low']],
         ['&',['low','high','low']],
         ['&',['low','veryhigh','medium']],
         ['&',['medium','low','low']],
         ['&',['medium','medium','low']],
         ['&',['medium','high','medium']],
         ['&',['medium','veryhigh','high']],
         ['&',['high','low','medium']],
         ['&',['high','medium','medium']],
         ['&',['high','high','high']],
         ['&',['high','veryhigh','high']]]

values = [[input1[1],8.0],[input2[1],10.0]]

f = fuzzy()
f.setInputs(inputs,functionsInputs)
f.setOutput(output,functionOutput)
f.setRules(rules)
#f.printModel()
res = f.run(values,output[1])
print("output: "+str(res))
