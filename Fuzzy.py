import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as c

class fuzzy:
    inputs = []
    output = []
    rules = []
    controlV = c.ControlSystem()
    tipping = c.ControlSystemSimulation(controlV)

    def setInputs(self,inp,func):
        for i in inp:
            t = tuple(i[0])
            self.inputs.append(c.Antecedent(np.arange(t[0],t[1],t[2]),i[1]))
        self.doFunctionsInput(func)

    def setOutput(self,inp,func):
        t = tuple(inp[0])
        self.output.append(c.Consequent(np.arange(t[0],t[1],t[2]), inp[1]))
        self.doFunctionOutput(func)

    def doFunctionOutput(self,func):
        for n in func:
            out = self.output[0]
            if n[0] == 'trimp':
                out[n[1]]= fuzz.trimf(out.universe,n[2])
            if n[0] == 'tramp':
                out[n[1]]= fuzz.trapmf(out.universe,n[2])

    def doFunctionsInput(self,func):
        count = 0
        for f in func:
            for n in f:
                inp = self.inputs[count]
                if n[0] == 'trimp':
                    inp[n[1]]= fuzz.trimf(inp.universe,n[2])
                if n[0] == 'tramp':
                    inp[n[1]]= fuzz.trapmf(inp.universe,n[2])
            count+=1

    def setRules(self,rules):
        for r in rules:
            antecedent = self.getAntecedent(r)
            concequent = self.getCoonsequent(r)
            self.rules.append(c.Rule(antecedent,concequent))
        self.controlV = c.ControlSystem(self.rules)
        self.tipping = c.ControlSystemSimulation(self.controlV)

    def getAntecedent(self,r):
        antS = r[1][0:len(r[1])-1]
        rs = []
        count = 0
        for ant in antS:
            rs.append(self.inputs[count][ant])
            count+=1
        res = [rs[0]]
        rs.pop(0)
        for antr in rs:
            if r[0]=='&':
                res[0]= res[0] & antr
            if r[0]=='|':
                res[0]= res[0] | antr
        return res[0]

    def getCoonsequent(self,r):
        conq = r[1][len(r[1])-1]
        return self.output[0][conq]

    def run(self,values,out):
        for v in values:
            self.tipping.input[v[0]]=v[1]
        self.tipping.compute()
        return self.tipping.output[out]

    def printModel(self):
        print("\ninputs: "+str(self.inputs))
        print("\noutputs: "+str(self.output))
        print("\nRules:\n")
        for r in self.rules:
            print(str(r))
