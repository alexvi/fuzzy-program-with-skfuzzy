from Fuzzy import *
from Rules import *

#inputs
input1 = [(0,101,1),'visual']
input2 = [(0,101,1),'audio']
input3 = [(0,11,1),'motriz']
input4 = [(0,101,1),'fisica']
#functions of inputs
# function, name, rangue[]
functionsInput1 = [['tramp','nulo', [0,0,5,10]],
                   ['trimp','verylow', [5,15,25]],
                   ['trimp','low', [15,25,35]],
                   ['tramp','medium', [30,35,55,60]],
                   ['trimp','high', [55,70,85]],
                   ['tramp','veryhigh',[80,85,100,100]]]

functionsInput2 = [['tramp','nulo',[0,0,10,20]],
                   ['trimp','low',[15,30,45]],
                   ['tramp','medium',[40,50,60,70]],
                   ['trimp','high',[60,75,90]],
                   ['tramp','veryhigh',[80,90,100,100]]]

functionsInput3 = [['trimp','nulo',[0,2,4]],
                   ['trimp','parcial',[2,5,8]],
                   ['tramp','total',[6,8,10,10]]]

functionsInput4 = [['tramp','nulo',[0,0,5,15]],
                   ['tramp','medium',[10,30,50,70]],
                   ['tramp','high',[50,70,100,100]]]

inputs = [input1,input2,input3,input4]
functionsInputs = [functionsInput1,functionsInput2,functionsInput3,functionsInput4]

#output
output = [(0,101,1),'res']
#function of output
functionOutput = [['tramp','noapto',[0,10,30,40]],
                  ['tramp','evaluation',[30,40,60,70]],
                  ['tramp','apto',[50,70,100,100]]]

r = rules()
#Rules conditional , values[logicalCombination,[Antecedent1,...,AntecedentN,Consequent]]
rules = r.getRules()

#in: [value input1, value input2, value input3,...]
values = [90.0, 45.0, 75.0, 60.0]
inputf = [[input1[1],values[0]],[input2[1],values[1]],[input3[1],values[2]],[input4[1],values[3]]]

f = fuzzy()
f.setInputs(inputs,functionsInputs)
f.setOutput(output,functionOutput)
f.setRules(rules)
#f.printModel()
res = f.run(inputf,output[1])
print("in: visual= "+str(inputf[0][1])+" - audio= "+str(inputf[1][1])+
       " - motriz= "+str(inputf[2][1])+" - fisica= "+str(inputf[3][1]))
print("output: "+str(res))
if(res>60):
    print("Resultado: APTO")
else:
     if res>40:
         print("Resultado: SEGUNDA EVALUACION")
     else:
         print("Resultado: NO APTO")
